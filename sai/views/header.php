<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>AMR Administration</title>
 		<script type="text/javascript" src="/dist/index.js"  charset="utf-8"></script>
		<script type="text/javascript" src="/js/functions.js"></script>
		<script type="text/javascript" src="/js/js.cookie.js"></script>
		<script type="text/javascript" src="/sai/js/saiFunctions.js"></script>

		<link rel="icon" href="data:,">
		<link rel="stylesheet" type="text/css" href="/dist/index.css">
		<link rel="stylesheet" type="text/css" href="/sai/css/simpleAdminInterface.css" />
	</head>
	<body>