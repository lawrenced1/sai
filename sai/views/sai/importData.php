<?php
	use nuCore\Debug;

	$aryPreview = array();

	$aryDatabaseSettings = $objRegistry->get( 'database' );


	/**
	 * Change finished importing to say DONE and put a green checkmark on the screen
	 *
	 * 	TODO:
	 * Add upload to upload a CSV,SQL file to import data
	 * Button1: Import from Server
	 * Button2: Import from backup file
	 * Button3: Upload File
	 * Button4: Import from upload
	 */
?>
<script type="text/javascript">
	var intervalID;
	var blnStopProcess = false;
	var engine = <?php echo ( isset( $aryDatabaseSettings['import']['engine'] ) ) ? json_encode($aryDatabaseSettings['import']['engine']) : ''; ?>;
	var errorIcon = '<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>';

	$(document).ready( function()
	{
		var txtBoxData = $("#txtPreview").val();

		$("#prbOverall").progressbar({ value: 0 });
		$("#prbSteps").progressbar({ value: 0 });

		$("#chkHeader").change( function(e)
		{
			if ($(this).is(':checked'))
			{
				var temp = $("#txtPreview").val().split("\n");
				temp = temp.slice(1,temp.length);
				temp = temp.join("\n");
				$("#txtPreview").val(temp);
			}
			else
			{
				$("#txtPreview").val(txtBoxData);
			}
		});

		$("#importData").click(function(e)
		{
			console.log( 'Import clicked' ) ;
			$.ajax(
			{
				type: 'POST',
				url: '/admin/importDataAjax/' + ( ($("#chkHeader").is(":checked")) ? "header" : ""),
				data: postData,
				dataType: 'json',
				statusCode: {
					500: function( response )
					{
						console.log( response );
					}
				}
			})
			.done (function (response)
			{
				//if our json structure is as expected
				if (response.success)
				{
					if (response.success == "true")
					{
						$('#divImportAjaxMessages').html(response.data);
						$("#divImportAjaxMessages").show();
					}
					else
					{
						$('#divImportAjaxMessages').html(response.stringify);
						$("#divImportAjaxMessages").show();
					}
				}
			})
			.error (function (xhr, ajaxOptions, thrownError)
			{
				console.log( 'importData click ');
				console.log( xhr );
				console.log(ajaxOptions);
				console.log( thrownError );
				$('#divImportAjaxMessages').html(xhr.statusText + ':' + thrownError);
				$("#divImportAjaxMessages").show();
			});
		});

		$("#btnImportDB").on( "click", function(e)
		{
			//Setup the progress bars
			$("#prbOverall").progressbar({ max: 3 });
			$("#prbOverall").progressbar({ value: 1 });

			$("#prbSteps").progressbar({ max: 100 });
			$("#prbSteps").progressbar({ value: false });

			$("#prbStepsLabel").text("Getting record count...")
			
			
			if ( ! $("#chkBypassDownload").is( ':checked' ) )
			{
				getRowCount();
			}
			else
			{
				importLocalData();
			}
		});
	});

	function getRowCount()
	{
		console.log( 'getRowCount() ');
		//Get Row count
		//
		$.ajax(
		{
			type: 'GET',
			url : '/' + engine + '/getRowCount',
			dataType : 'json',
			statusCode: {
				500: function( response )
				{
					console.log( '500 response: ' + response );
				}
			}
		})
		.done (function( response )
		{
			if ( response.success === 'true' )
			{
				$("#prbStepsLabel").text("Found " + response.data[0].RECORDCOUNT + " records for import...");
				$("#prbSteps").progressbar("option", "max", Number(response.data[0].RECORDCOUNT));

				getMemberData();
			}
			else
			{
				$("#prbStepsLabel").text("Error gettting row count.");
				reportError( 'divImportAjaxMessages', response.errors );
			}
			
		})
		.error( function( xhr, ajaxOptions, thrownError )
		{
			clearInterval(intervalID);
			reportError( 'divImportAjaxMessages', xhr.statusText + ':' + thrownError );
			console.log( xhr );
			console.log( 'ajaxoptions ' + ajaxOptions);
			console.log( 'thrownerror ' + thrownError );
		});
	}

	function getMemberData()
	{
		$("#prbOverall").progressbar("option", "value", 2);
		
		if ( ! $("#chkBypassDownload").is( ':checked' ) )
		{
			$.ajax(
			{
				type: 'GET',
				url : '/' + engine + '/getMemberData',
				dataType : 'json',
				async: true
			})
			.done( function( response )
			{
				if ( response.success )
				{
					if ( response.success === 'true' )
					{
						$("#prbStepsLabel").text("Downloaded records...");
						$("#prbSteps").progressbar("option", "max", Number(response.data[0].RECORDCOUNT));
						clearInterval(intervalID);

						importLocalData();
					}
					else
					{
						clearInterval( intervalID );
						reportError( 'divImportAjaxMessages', response.errors );
					}
				}
				else
				{
					/**
					 * Unexpected format
					 */
					reportError( 'divImportAjaxMessages', 'Unexpected response ' + JSON.stringify( resposne ) );
				}
				
			})
			.error( function( xhr, ajaxOptions, thrownError)
			{
				clearInterval(intervalID);
				console.log(ajaxOptions);
				$("#divImportAjaxMessages").addClass('ui-state-error ui-corner-all');
				$('#divImportAjaxMessages').html( errorIcon + xhr.statusText + ':' + thrownError );
				$("#divImportAjaxMessages").show();
			});

			intervalID = setInterval(processStatus, 5000);
		}
		else
		{
			importLocalData();
		}

		
		
	}


/********************************************************
need to change this to new ajax methods
*******************************************************/

	function importLocalData()
	{
		$("#prbOverall").progressbar("option", "value", 3);

		$.ajax(
		{
			type: 'GET',
			url : '/admin/importLocalData',
			dataType : 'json',
			success: function(response)
			{
				console.log( response );
				if ( response.success === 'true' )
				{
					$("#prbStepsLabel").text("Finished importing ");
					$("#prbSteps").progressbar( "option", "value", Number( response.data ) );
					
					//console.log( 'Interval ID Before from import: {' + intervalID + '}' );
					clearInterval(intervalID);
					//console.log( 'Interval ID After from import: {' + intervalID + '}' );
				}
				else
				{
					clearInterval(intervalID);
					console.log('importLocalData False:' + response.errors);
					$("#divImportAjaxMessages").addClass('ui-state-error ui-corner-all');
					$("#divImportAjaxMessages").html( response.errors );
					$("#divImportAjaxMessages").show();
				}
			},
			error: function(xhr, ajaxOptions, thrownError)
			{
				clearInterval(intervalID);
				console.log('importLocalData Error' + ajaxOptions);
				$("#divImportAjaxMessages").addClass('ui-state-error ui-corner-all');
				$('#divImportAjaxMessages').html( errorIcon + xhr.statusText + ':' + thrownError );
				$("#divImportAjaxMessages").show();
			}
		});

		intervalID = setInterval(processStatus, 5000);
	}

	function processStatus(strPrepend)
	{
		//console.log( 'Interval ID: {' + intervalID + '}' );
		$.ajax(
		{
			type: 'GET',
			url : '/admin/processStatus',
			dataType : 'json',
			success: function(response)
			{
				if (response.data)
				{
					$( "#prbSteps" ).progressbar( "option", "max", response.data.max);
					$( "#prbSteps" ).progressbar( "option", "value", Number(response.data.current));

					$("#prbStepsLabel").text('Processing ' + response.data.current + ' of ' + response.data.max + '.');				

					if (response.data.current == response.data.max)
					{
						console.log( 'Interval ID Before: {' + intervalID + '}' );
						clearInterval(intervalID);
						console.log( 'Interval ID After: {' + intervalID + '}' );
						//$("#progressbar > div").html("Complete");
					}
					
				}
				console.log(response);
			},
			error: function(xhr, ajaxOptions, thrownError)
			{
				console.log(ajaxOptions);
				$("#divImportAjaxMessages").addClass('ui-state-error ui-corner-all');
				$('#divImportAjaxMessages').html( errorIcon + xhr.statusText + ':' + thrownError );
				$("#divImportAjaxMessages").show();
			}
		});
	}
</script>
<style>
#ImportHeader
{
	text-align: center;
	margin-left: auto;
	margin-right: auto;
}

#ImportContent
{
	background: #fff;
	color: #444;
	font-family: "Open Sans", sans-serif;
	/*margin: 140px auto 25px;*/
	margin-left: auto;
	margin-right: auto;
	padding: 20px 20px 10px 20px;
	max-width: 700px;
	-webkit-font-smoothing: subpixel-antialiased;
	-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.13);
	box-shadow: 0 1px 3px rgba(0,0,0,0.13);
	text-align: center;
	;
}

.progress
{
	text-align: center;
	text-shadow: 1px 1px 0 #fff;
	height: 10px;
}

.progress-label
{
	padding-top: 10px;
	/*padding-bottom: 10px;*/
}


</style>
<div id="ImportWrapper">
	<div id="ImportHeader"><h1>Import Data</h1></div>
	<div id="ImportContent">
	<?php Debug::VarDump( $objRegistry ); ?>
	<?php if (isset($objRegistry->config['database']['import']['engine'])) { ?>
		<?php if ($objRegistry->config['database']['import']['engine'] != 'csv'){ ?>
			
			<div>
				<input id="btnImportDB" type="button" value="Import Records"/><br/><span style="font-size: smaller;"><input id="chkBypassDownload" type="checkbox" />Skip download because we are Offline</span>
			</div>
			<div id="prbOverallLabel" class="progress-label">Overall</div>
			<div id="prbOverall" class="progress"></div>
			<div id="prbStepsLabel" class="progress-label">Steps</div>
			<div id="prbSteps" class="progress"></div>
			
		<?php 
		} 
		else 
		{
			//Read first five lines of the import.csv file
			if ($fh = fopen(ROOT.DS.'tmp'.DS.'upload'.DS.'import.csv', 'r'))
			{
				while (!feof($fh) && count($aryPreview) <= 5)
				{
					$aryPreview[] = fgets($fh, 1024);
				}

				fclose($fh);
			}
		?>
			CSV Preview
			<br/>
			<textarea id="txtPreview" style="width:100%" wrap="off" rows="7"><?php foreach ($aryPreview as $line){ print $line; } ?></textarea>
			Does the first line contain headers? <input id="chkHeader" type="checkbox" value="n" />Yes
			<br />
			<div style="text-align: right;"><input id="btnImportCSV" type="button" value="Import Data"/></div>
		<?php } ?>
	<?php } else { ?>
		<h1>No engine specified. Check your Import Settings.</h1>
	<?php } ?>
	</div>
</div>
<div id="divImportAjaxMessages"></div>