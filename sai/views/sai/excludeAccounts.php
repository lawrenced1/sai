<?php

?>

<script>
<!--
	var intervalID;

	$(document).ready( function()
	{
		$("#progressBar").hide();
		$("#progressBar").progressbar({ value: false });
		
		$("#frmImport").submit( function( event )
		{
			event.preventDefault();

			$("#progressBar").show();

			var file_data = new FormData( $("#frmImport")[0] );

			$.ajax (
			{
				url: 'registrations/excludeAccounts',
				type: 'POST',
				data: file_data,
				enctype: 'multipart/form-data',
				dataType: 'json',
				processData: false,
				contentType: false
			})
			.done (function (response)
			{
				//if our json structure is as expected
				if (response.success)
				{
					$("#progressBar").hide();

					if (response.success == "true")
					{
						$('#divExcludeAjaxMessages').html(response.data);
						$("#divExcludeAjaxMessages").show();
						clearInterval(intervalID);
					}
					else
					{
						$("#divExcludeAjaxMessages").addClass('ui-state-error ui-corner-all');
						$('#divExcludeAjaxMessages').html(response.errors);
						$("#divExcludeAjaxMessages").show();
					}
				}
			})
			.error (function (xhr, ajaxOptions, thrownError)
			{
				clearInterval(intervalID);
				console.log(ajaxOptions);
				$('#divExcludeAjaxMessages').html(xhr.statusText + ':' + thrownError);
				$("#divExcludeAjaxMessages").show();
			});

			intervalID = setInterval(processStatus, 5000);
		});

		$("#btnPreregister").on( "click", function( event )
		{
			if ( ! window.confirm("Are you sure you wish to pre-register the keyCustomerCode accounts?") )
			{
				return;
			}

			event.preventDefault();

			$("#progressBar").show();

			$.ajax({
				type: "GET",
				url: "/registrations/preRegisterKeyAccounts",
				dataType : "JSON",
			})
			.done( function( response )
			{
				/**
				 * Test response from server in our expected structure
				 */
				
				if ( response.success)
				{
					/**
					 * Test response success
					 */
					$("#progressBar").hide();
					
					if (response.success == "true")
					{
						$('#divExcludeAjaxMessages').html(response.data);
						$("#divExcludeAjaxMessages").show();
						clearInterval(intervalID);
					}
					else
					{
						$("#divExcludeAjaxMessages").addClass('ui-state-error ui-corner-all');
						$('#divExcludeAjaxMessages').html(response.errors);
						$("#divExcludeAjaxMessages").show();
					}
				}
			})
			.error (function (xhr, ajaxOptions, thrownError)
			{
				clearInterval(intervalID);
				console.log(ajaxOptions);
				$('#divExcludeAjaxMessages').html(xhr.statusText + ':' + thrownError);
				$("#divExcludeAjaxMessages").show();
			});

			intervalID = setInterval(processStatus, 5000);
		});
	});

	function processStatus(strPrepend)
	{
		//console.log( 'Interval ID: {' + intervalID + '}' );
		$.ajax(
		{
			type: 'GET',
			url : '/admin/processStatus',
			dataType : 'json',
			success: function(response)
			{
				if (response.data)
				{
					$( "#prbSteps" ).progressbar( "option", "max", response.data.max);
					$( "#prbSteps" ).progressbar( "option", "value", Number(response.data.current));

					$("#prbStepsLabel").text('Processing ' + response.data.current + ' of ' + response.data.max + '.');				

					if (response.data.current == response.data.max)
					{
						console.log( 'Interval ID Before: {' + intervalID + '}' );
						clearInterval(intervalID);
						console.log( 'Interval ID After: {' + intervalID + '}' );
						//$("#progressbar > div").html("Complete");
					}
					
				}
				console.log(response);
			},
			error: function(xhr, ajaxOptions, thrownError)
			{
				console.log(ajaxOptions);
				$("#divExcludeAjaxMessages").addClass('ui-state-error ui-corner-all');
				$('#divExcludeAjaxMessages').html( errorIcon + xhr.statusText + ':' + thrownError );
				$("#divExcludeAjaxMessages").show();
			}
		});
	}
-->
</script>
<style>
.progress
{
	text-align: center;
	text-shadow: 1px 1px 0 #fff;
	height: 10px;
}
</style>
<form action="/admin/excludeAccounts" method="post" enctype="multipart/form-data" name="frmImport" id="frmImport">
	<input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
	<h2>Upload a file to mark accounts as excluded accounts</h2>
	<p>Enter File with account numbers to flag as excluded to prevent printing with members:</p>
	<p><input name="importFile" type="file"></p>
	<p><input type="submit" value="Upload File"></p>
	<hr/>
	<h4>Additional functions</h4>
	<p><input type="button" id="btnPreregister" name="btnPreregister" value="Pre-Register accounts flagged with KeyCustomerCode?"></p>
	<p><input type="button" id="btnSetBatch" name="btnSetBatch" value="Set Pre-Register Batch to prevent printing"></p>
</form>
<ul>
<ol>Upload file - show total number of rows seen</ol>
<ol>Click Preregister button</ol>
<ol>Click Set the Pre-registration batch button</ol>
</ul>
<div id="progressBar" class="progress"></div>
<div id="divExcludeAjaxMessages" style="display:none;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span></div>