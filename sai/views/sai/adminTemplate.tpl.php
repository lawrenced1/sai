<?php

?>
<script type="text/javascript">
	var intervalID;
	var errorIcon = '<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>';
	var errorClass = 'ui-state-error ui-corner-all';

	$(document).ready( function()
	{
		$("#").click(function(e)
		{
			$.ajax(
			{
				type: '',
				url: '',
				data: ,
				dataType: 'json',
			})
			.done ( function (response)
			{
				/**
				 * Test to see if our response is in the format we expect
				 */
				if (response.success)
				{
					if (response.success == "true")
					{
						/**
						 * Handle successful action
						 */
					}
					else
					{
						/**
						 * Handle Error or report
						 */
						reportError( JSON.stringify( response ) );
					}
				}
				else
				{
					/**
					 * Wrong format
					 */
					reportError( 'Response is not in expected format: ' + JSON.stringify( response ) );
				}
			})
			.fail ( function( xhr, ajaxOptions, thrownError )
			{
				/**
				 * Ajax error
				 */
				reportError( 'Ajax error: ' + xhr.statusText + ':' + thrownError );
			});
		});
	});

	function reportError( strErrorMessage )
	{
		if (typeof intervalID != 'undefined') clearInterval(intervalID);
		console.log( strErrorMessage );
		$("#ajaxMessages").addClass( errorClass );
		$('#ajaxMessages').html( errorIcon + strErrorMessage );
		$("#ajaxMessages").show();
	}
</script>
<style></style>
<div id="AdminWrapper">
	<div id="AdminHeader"><h1>{Title}</h1></div>

	<div id="AdminContent" class="admin">
		{Content}
	</div>
</div>
<div id="ajaxMessages"></div>