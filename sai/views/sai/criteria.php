<?php
	if ( isset( $objRegistry->config['criteria'] ) )
	{
		$aryCriteria = $objRegistry->config['criteria'];
	}
	else
	{
		/**
		 * Criteria is not set so we need our default
		 */
		$strForm = <<<HTML
HTML;
	}

	if ( ! empty( $aryFields ) )
	{
		$strFieldList = '<option></option>';
		foreach ($aryFields as $field)
		{
			$strFieldList .= '<option value="' . $field['Field'] . '">' . $field['Field'] . '</option>' . PHP_EOL;
		}
	}
?>
<script type="text/javascript">
	var intervalID;

	var JSONcritera = <?php print ( !empty ($aryCriteria ) && is_array($aryCriteria) ) ? "'" . json_encode($aryCriteria) . "';": "'';"; ?>

	$(document).ready( function()
	{
		getCriteria();

		$("#btnSave").click(function(e)
		{
			saveCriteria();
		});

		$("input.btnAddRow").on('click', function() 
		{
			var $tr    = $(this).closest('.tr_clone');
			var $clone = $tr.clone(true);
			$clone.find(':text').val('');
			$tr.after($clone);
			return false;
		});

		$("input.btnDeleteRow").on('click', function()
		{
			if ( $(".tr_clone").size() > 1 )
			{
				var tr = $(this).closest('tr');
				tr.css("background-color", "#FF3700");
				tr.fadeOut(400, function()
				{
					tr.remove();
				});
			}
			
			return false;
		});

		$("select.select_clone").on('change', function()
		{
			console.log( $(this).val() );
			var itemName = $(this).val();
			$(this).closest('tr').find('input, select, text, textarea').each( function()
			{
				if ( itemName )
				{
					$(this).attr("name", itemName + '-' + $(this).attr('data-clone-name') );
				}
				else
				{
					$(this).attr("name", '' );
				}
				
			});
		});

		$("#btnTest").on('click', function()
		{
			console.log ( $("#frmCriteria").values() );
		});
	});

	function getCriteria()
	{
		$.ajax(
		{
			type: 'GET',
			url: '/admin/getCriteria',
			dataType: 'json'
		})
		.done( function (response)
		{
			/**
			 * Test to see if our response is in the format we expect
			 */
			if (response.success)
			{
				if (response.success == "true")
				{
					/**
					 * Handle successful action
					 */
					//$("#frmCriteria").values( response.data );
					
					var originRow = $("#frmCriteria").find( '.tr_clone:first' );
					var workingClone = originRow.clone(true);

					/**
					 * Build the form based on returning data
					 */
					for ( var element in response.data )
					{
						if ( response.data.hasOwnProperty( element ) )
						{
							/**
							 * aryName array of sqlColumn and name for elements in form
							 * @type {array}
							 */
							var aryName = element.split( '-' );

							var dataCloneName = workingClone.find('[data-clone-name="' + aryName[1] + '"]');

							if (  dataCloneName.attr( "name" ) == null )
							{
								dataCloneName.attr( "name", element );
							}
							else
							{
								originRow.before( workingClone );
								workingClone = originRow.clone(true);
								dataCloneName = workingClone.find('[data-clone-name="' + aryName[1] + '"]');
								dataCloneName.attr( "name", element );
							}							
						}
					}

					/**
					 * Append last run through
					 */
					 originRow.before( workingClone );
					 originRow.remove();

					 $("#frmCriteria").values( response.data );

					getMemberCount();
				}
				else
				{
					/**
					 * Handle Error or report
					 */
					reportError( 'divAjaxMessages', JSON.stringify( response ) );
				}
			}
			else
			{
				/**
				 * Wrong format
				 */
				reportError( 'divAjaxMessages', 'Response is not in expected format: ' + JSON.stringify( response ) );
			}
		})
		.fail ( function( xhr, ajaxOptions, thrownError)
		{
			/**
			 * Ajax error
			 */
			reportError( 'divAjaxMessages', 'Ajax error: ' + xhr.statusText + ':' + thrownError );
		});
	}

	function saveCriteria()
	{
		
		$.ajax(
		{
			type: 'POST',
			url: '/admin/saveCriteria',
			data: $("#frmCriteria").serialize(),
			dataType: 'json'
		})
		.done( function (response)
		{
			/**
			 * Test to see if our response is in the format we expect
			 */
			if (response.success)
			{
				if (response.success == "true")
				{
					/**
					 * Handle successful action
					 */
					reportInfo( 'divAjaxMessages', 'Successfully saved criteria.' );
					getMemberCount();
				}
				else
				{
					/**
					 * Handle Error or report
					 */
					reportError( JSON.stringify( response ) );
				}
			}
			else
			{
				/**
				 * Wrong format
				 */
				reportError( 'divAjaxMessages', 'Response is not in expected format: ' + JSON.stringify( response ) );
			}
		})
		.fail ( function( xhr, ajaxOptions, thrownError)
		{
			/**
			 * Ajax error
			 */
			reportError( 'divAjaxMessages', 'Ajax error: ' + xhr.statusText + ':' + thrownError );
		});
	}

	function getMemberCount()
	{
		
		$.ajax(
		{
			type: 'GET',
			url: '/registrations/GetMemberCount',
			dataType: 'json'
		})
		.done( function (response)
		{
			/**
			 * Test to see if our response is in the format we expect
			 */
			if (response.success)
			{
				if (response.success == "true")
				{
					/**
					 * Handle successful action
					 */
					$("#divMemberCount").html( '<h2>Member Count: ' + response.data[0].memberCount + '</h2>');
				}
				else
				{
					/**
					 * Handle Error or report
					 */
					reportError( 'divAjaxMessages', JSON.stringify( response ) );
				}
			}
			else
			{
				/**
				 * Wrong format
				 */
				reportError( 'divAjaxMessages', 'Response is not in expected format: ' + JSON.stringify( response ) );
			}
		})
		.fail ( function( xhr, ajaxOptions, thrownError)
		{
			/**
			 * Ajax error
			 */
			reportError( 'divAjaxMessages', 'Ajax error: ' + xhr.statusText + ':' + thrownError );
		});
	}

	

	/* jQuery.values: get or set all of the name/value pairs from child input controls   
	 * @argument data {array} If included, will populate all child controls.
	 * @returns element if data was provided, or array of values if not
	 * http://stackoverflow.com/questions/1489486/jquery-plugin-to-serialize-a-form-and-also-restore-populate-the-form/1490431#1490431
	*/

	$.fn.values = function(data) {
		console.log(data);
		var els = $(this).find(':input').get();

		if(typeof data != 'object') {
			// return all data
			data = {};

			$.each(els, function() {
				if (this.name && !this.disabled && (this.checked
								|| /select|textarea/i.test(this.nodeName)
								|| /text|hidden|password/i.test(this.type))) {
					data[this.name] = $(this).val();
				}
			});
			return data;
		} else {
			$.each(els, function() {
				if (this.name && data[this.name]) {
					if(this.type == 'checkbox' || this.type == 'radio') {
						$(this).attr("checked", (data[this.name] == $(this).val()));
					} else {
						$(this).val(data[this.name]);
					}
				}
			});
			return $(this);
		}
	};
</script>
<style>
	#CriteriaHeader
	{
		text-align: center;
		margin-left: auto;
		margin-right: auto;
	}

	#divMemberCount
	{
		text-align: center;
		padding-bottom: 10px;
	}

	#divCriteriaNotes
	{
		margin-top:20px;
		padding: 5px;
		margin-left:auto;
		margin-right:auto;
		width:65%;
		max-width: 720px;
	}

	#divCriteriaNotes p
	{

	}

	#divCriteriaNotes ul
	{
		list-style: none;
		padding-left: 0;
	}

	#divCriteriaNotes li
	{
		list-style: none;

	}

	#divCriteriaNotes li .ui-icon
	{
		display: inline-block;
	}

</style>
<div id="CriteriaWrapper">
	<div id="CriteriaHeader"><h1>Set Criteria</h1></div>

	<div id="CriteriaContent" class="admin">
		<div id="divMemberCount"></div>
		<form id="frmCriteria">
			<?php print ( !empty( $strForm ) ) ? $strForm : ''; ?>
			<table style="width:100%;" id="tblCriteria">
				<tr>
					<th>Column Name</th>
					<th>Values</th>
					<th>Error Message</th>
					<th></th>
				</tr>
				<tr class="tr_clone">
					<td><select class="select_clone" data-clone-name="Columns"><?php print ( !empty( $strFieldList ) ? $strFieldList : '<No Data>' );?></select></td>
					<td><input type="text" data-clone-name="Values"/></td>
					<td><textarea rows="2" cols="30" data-clone-name="ErrorMessage"></textarea></td>
					<td><input type="button" class="btnAddRow" value="+" data-clone-name="Add"/><input type="button" class="btnDeleteRow" value="-" data-clone-name="Delete"/></td>
			</table>
			<div id="divCriteriaNotes" class="ui-state-highlight ui-corner-all">
			<ul>
				<li><span class="ui-icon ui-icon-note"></span>Values can be a single value like Y or 5. Both without quotes.</li>
				<li><span class="ui-icon ui-icon-note"></span>Values can also be multiple values separated by a comma; 1,3,5,18.</li>
			</ul>
			</div>
			<p><input type="button" id="btnSave" value="Save" /><input type="reset" id="btnReset" value="Reset" /></p>
			
		</form>
	</div>
</div>