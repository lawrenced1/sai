<?php

	$strMemberServerName = '';
	$strMemberServerDatabase = '';
	$strMemberServerLogin = '';
	$strMemberServerPassword = '';


	//Load local variables via config file
	if (!empty($objRegistry))
	{
		$aryDatabaseSettings = $objRegistry->get( 'database' );
		//In future add one for engine when we are able to select from multiple member server databases
		$strMemberServerName = !empty($aryDatabaseSettings['import']['servernameip']) ? $aryDatabaseSettings['import']['servernameip'] : '';
		$strMemberServerDatabase = !empty($aryDatabaseSettings['import']['database']) ? $aryDatabaseSettings['import']['database'] : '';
		$strMemberServerLogin = !empty($aryDatabaseSettings['import']['user']) ? $aryDatabaseSettings['import']['user'] : '';
		$strMemberServerPassword = !empty($aryDatabaseSettings['import']['password']) ? $aryDatabaseSettings['import']['password'] : '';

		$strEngine = !empty($aryDatabaseSettings['import']['engine']) ? $aryDatabaseSettings['import']['engine'] : '';

	}
?>
<style>
	#SettingsHeader
	{
		text-align: center;
		margin-left: auto;
		margin-right: auto;
	}

	.ui-progressbar
	{
		position: relative;
	}

	.progress-label
	{
		position: absolute;
		left: 30%;
		top: 4px;
		font-weight: bold;
		text-shadow: 1px 1px 0 #fff;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(e)
	{
		$("#csvUpload").hide();

		$("#importEngine").change( function (e)
		{
			switch($(this).val())
			{
				case 'nisc':
					$.ajax(
					{
						type: 'GET',
						url: '/admin/changeDataProvider/nisc',
						dataType: 'json',
						success: function (response)
						{
							//If our JSON object is structured like we expect
							if (response.success)
							{
								//Check for success message
								if (response.success == "true")
								{
									//True
									$("#ajaxMessages").html(response.data);
									$("#ajaxMessages").show();
								}
								else
								{
									//False
									$("ajaxMessages").html(response.errors);
									$("#ajaxMessages").show();
								}
							}
							else
							{
								//Not the structure we were expecting
								$('#ajaxMessages').html(response.stringify);
								$("#ajaxMessages").show();
							}
						},
						error: function (xhr, ajaxOptions, thrownError)
						{
							$('#ajaxMessages').html(xhr.responseText);
							$("#ajaxMessages").show();
						}
					});

					$("#csvUpload").hide();
					$("#serverSettings").show();

					break;
				case 'sedc':
					$.ajax(
					{
						type: 'GET',
						url: '/admin/changeDataProvider/sedc',
						dataType: 'json',
						success: function (response)
						{
							//If our JSON object is structured like we expect
							if (response.success)
							{
								//Check for success message
								if (response.success == "true")
								{
									//True
									$("#ajaxMessages").html(response.data);
									$("#ajaxMessages").show();
								}
								else
								{
									//False
									$("ajaxMessages").html(response.errors);
									$("#ajaxMessages").show();
								}
							}
							else
							{
								//Not the structure we were expecting
								$('#ajaxMessages').html(response.stringify);
								$("#ajaxMessages").show();
							}
						},
						error: function (xhr, ajaxOptions, thrownError)
						{
							$('#ajaxMessages').html(xhr.responseText);
							$("#ajaxMessages").show();
						}
					});
					$("#csvUpload").hide();
					$("#serverSettings").show();
					break;
				case 'csv':
					$.ajax(
					{
						type: 'GET',
						url: '/admin/changeDataProvider/csv',
						dataType: 'json',
						success: function (response)
						{
							//If our JSON object is structured like we expect
							if (response.success)
							{
								//Check for success message
								if (response.success == "true")
								{
									//True
									$("#ajaxMessages").html(response.data);
									$("#ajaxMessages").show();
								}
								else
								{
									//False
									$("ajaxMessages").html(response.errors);
									$("#ajaxMessages").show();
								}
							}
							else
							{
								//Not the structure we were expecting
								$('#ajaxMessages').html(response.stringify);
								$("#ajaxMessages").show();
							}
						},
						error: function (xhr, ajaxOptions, thrownError)
						{
							$('#ajaxMessages').html(xhr.responseText);
							$("#ajaxMessages").show();
						}
					});

					$("#csvUpload").show();
					$("#serverSettings").hide();
					break;
				default:
					$("#csvUpload").hide();
					$("#serverSettings").show();
			}
		});

		$('#btnSaveServerSettings').click( function (e)
		{
			if ($("#importEngine").val() === "")
			{
				alert ("You must select a database engine");
			}
			else
			{
				if ($("#importServerName").val() === "")
				{
					alert ("You must enter a server DNS name or IP address");
				}
				else
				{
					if ($("#importDatabase").val() === "")
					{
						alert ("You must specify the database to pull data from.")
					}
					else
					{
						if ($("#importLogin").val() === "")
						{
							alert ("You must supply a username to login with.");
						}
						else
						{
							if ($("#importPassword").val() === "")
							{
								alert ("You must supply a password to login with.");
							}
							else
							{
								var postData = "importEngine=" + $('#importEngine').val()
									+ '&importServerName=' + $('#importServerName').val()
									+ '&importDatabase=' + $('#importDatabase').val()
									+ '&importLogin=' + $('#importLogin').val()
									+ '&importPassword=' + $('#importPassword').val();

								$("#imgLoading").show();

								$.ajax(
								{
									type: 'POST',
									url: '/admin/setupMemberServerAjax',
									data: postData,
									dataType: 'json',
									success: function (response)
									{
										//if our json structure is as expected
										if (response.success)
										{
											if (response.success == "true")
											{
												$('#ajaxMessages').html(response.data);
												$("#ajaxMessages").show();
												$("#importServerName").css('background-color', 'green');
												$("#importDatabase").css('background-color', 'green');
												$("#importLogin").css('background-color', 'green');
												$("#importPassword").css('background-color', 'green');
											}
											else
											{
												$('#ajaxMessages').html(response.stringify);
												$("#ajaxMessages").show();
												$("#importServerName").css('background-color', 'red');
												$("#importDatabase").css('background-color', 'red');
												$("#importLogin").css('background-color', 'red');
												$("#importPassword").css('background-color', 'red');
												$("#ajaxMessages").css('font-weight', 'bold');
											}
										}

										$("#imgLoading").hide();
									},
									error: function (xhr, ajaxOptions, thrownError)
									{
										console.log(ajaxOptions);
										$('#ajaxMessages').html(xhr.statusText + ':' + thrownError);
										$("#ajaxMessages").show();
										$("#importServerName").css('background-color', 'red');
										$("#importDatabase").css('background-color', 'red');
										$("#importLogin").css('background-color', 'red');
										$("#importPassword").css('background-color', 'red');
										$("#ajaxMessages").css('font-weight', 'bold');

										$("#imgLoading").hide();
									}
								});
							}
						}
					}
				}
			}

		});

		$('#btnVerify').click( function (e)
		{
			$("#imgLoading").show();
			$.ajax(
			{
				type: 'GET',
				url: '/' + $("#importEngine").val() + '/verifyServerSettings',
				dataType: 'json',
				success: function (response)
				{
					//If our JSON object is structured like we expect
					if (response.success)
					{
						//Check for success message
						if (response.success == "true")
						{
							//True
							$('#imageVerify').html('<img src="/img/check.gif" />');
							$('#textVerify').html(response.data);
						}
						else
						{
							//False
							$('#imageVerify').html('<img src="/img/redx.gif" />');
							reportError( 'divSettingsAjaxMessages', response.errors );
						}
					}
					else
					{
						//Not the structure we were expecting
						console.log(response);
						$('#imageVerify').html('<img src="/img/redx.gif" />');
						reportError( 'divSettingsAjaxMessages', response.stringify );
					}
					$("#imgLoading").hide();
				},
				error: function (xhr, ajaxOptions, thrownError)
				{
					console.log(xhr);
					console.log(ajaxOptions);
					$('#imageVerify').html('<img src="/img/redx.gif" />');
					reportError( 'divSettingsAjaxMessages', xhr.responseText );
					$("#imgLoading").hide();
				}
			});
		});

		$("#errorMessage").hide();
		$("#progressbar").hide();

		$("#progressbar").progressbar(
		{
			value:false,
			change: function()
			{
				$(".progress-label").text(
				Math.floor($("#progressbar").progressbar("value") / $("#progressbar").progressbar("option", "max") * 100) + "%"
				);
			},

			complete: function()
			{
				$(".progress-label").text("Complete!");
				console.log($(':file').val());
			}
		});

		$('#FileUpload').change(function()
		{
			var file = this.files[0];
			var name = file.name;
			var size = file.size;
			var type = file.type;
			//Your validation

			console.log(file);
			console.log(name);
			console.log(size);
			console.log(type);
		});

		//Now the Ajax submit with the button's click:

		$('#btnFileUpload').click(function()
		{
			$("#progressbar").show();

			var formData = new FormData($('#frmFileUpload')[0]);

			$.ajax(
			{
				url: '/file/fileUploadAjax/import.csv',  //Server script to process data
				type: 'POST',
				xhr: function()
				{
					// Custom XMLHttpRequest
					var myXhr = $.ajaxSettings.xhr();

					if (myXhr.upload)
					{ // Check if upload property exists
						myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
					}

					return myXhr;
				},
				//Ajax events

				success: function (response)
				{
					if(response.success)
					{
						$("#errorMessage").hide();
					}
					else
					{
						$("#errorMessage").show();
						$(".progress-label").text("Loading failed");
						$("#errorMessageText").html(response.errors);
					}
				},

				error: function (xhr, ajaxOptions, thrownError)
				{
					$("#errorMessage").show();
					$(".progress-label").text("Loading failed");
					$("#errorMessageText").html(thrownError + xhr.responseText);
				},
				// Form data
				data: formData,
				//Options to tell jQuery not to process data or worry about content-type.
				cache: false,
				contentType: false,
				processData: false
			});
		});
	});

	<?php if ($strEngine == 'csv') { ?>
		$("#importEngine").trigger("change");
	<?php } ?>

	function progressHandlingFunction(e)
	{
		if(e.lengthComputable)
		{
			//$('progress').attr({value:e.loaded,max:e.total});
			$("#progressbar").progressbar("option", "max", e.total);
			$("#progressbar").progressbar("option", "value", e.loaded);
		}
	}
</script>
<div id="SettingsWrapper">
	<div id="SettingsHeader"><h1>Import Settings</h1></div>

	<div id="SettingsContent" class="admin">
		<table class="admin">
			<tr>
				<th colspan="2" align="center">Select your data provider and fill in the values</th>
			</tr>
			<tr>
				<td align="right" style="width:50%">Data Provider</td>
				<td><select name="importEngine" id="importEngine">
				<option value="nisc" <?php if (!empty($strEngine) && $strEngine == 'nisc') print 'SELECTED'; ?> >NISC</option>
				<option value="sedc" <?php if (!empty($strEngine) && $strEngine == 'sedc') print 'SELECTED'; ?> >SEDC</option>
				<option value="csv"  <?php if (!empty($strEngine) && $strEngine == 'csv') print 'SELECTED'; ?> >CSV</option>

				<!--<option value="mysql">MySQL</option>
				<option value="mssql">Microsoft SQL</option>-->
				</select></td>
			</tr>
		</table>
		<br/>
		<table id="serverSettings">
			<tr>
				<td align="right" style="width:50%">Server Name or IP</td>
				<td><input id="importServerName" type="text" value="<?php print $strMemberServerName; ?>" /></td>
			</tr>
			<tr>
				<td align="right">DataSource Database Name</td>
				<td><input id="importDatabase" type="text" value="<?php print $strMemberServerDatabase; ?>"></td>
			</tr>
			<tr>
				<td align="right">DataSource Login</td>
				<td><input id="importLogin" type="text" value="<?php print $strMemberServerLogin; ?>"></td>
			</tr>
			<tr>
				<td align="right">DataSource Password</td>
				<td><input id="importPassword" type="password" value="<?php print $strMemberServerPassword; ?>"></td>
			</tr>
			<tr>
				<td align="right">&nbsp;</td>
				<td><input type="submit" id="btnSaveServerSettings" value="Save Server Settings"></td>
			</tr>
			<tr>
				<td align="right"><div id="imageVerify" style="float:left"></div><div id="textVerify"></div></td>
				<td><input type="submit" name="btnVerify" id="btnVerify" value="Verify Server Settings"><img id="imgLoading" name="imgLoading" src="/img/ajax-loader.gif" width="15" height="15" style="padding-left:10px;display:none;" /></td>
			</tr>
		</table>
		<table id="csvUpload">
			<tr>
				<td align="right" style="width:50%">File to upload</td>
				<td>
					<form id="frmFileUpload" name="frmFileUpload" enctype="multipart/form-data">
						<input id="FileUpload" name="FileUpload" type="file" /><br/>
						<input id="btnFileUpload" name="btnFileUpload" type="button" value="Upload" />
					</form>
					<div id="progressbar"><div class="progress-label">Loading...</div></div>
					<div id="errorMessage" class="ui-state-error ui-corner-all" style="padding: .3em;">
						<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
						<span id="errorMessageText"></span>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>
<div id="divSettingsAjaxMessages"></div>