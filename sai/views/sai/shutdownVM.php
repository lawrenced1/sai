<?php

?>
<script type="text/javascript">
	var intervalID;
	var errorIcon = '<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>';
	var errorClass = 'ui-state-error ui-corner-all';

	$(document).ready( function()
	{
		$("#btnRestart").click(function(e)
		{
			$.ajax(
			{
				type: 'POST',
				url: '/admin/shutdownVM',
				data: {'action':'restart'},
				dataType: 'json',
			})
			.done ( function (response)
			{
				/**
				 * Test to see if our response is in the format we expect
				 */
				if (response.success)
				{
					if (response.success == "true")
					{
						/**
						 * Handle successful action
						 */
					}
					else
					{
						/**
						 * Handle Error or report
						 */
						reportError( JSON.stringify( response ) );
					}
				}
				else
				{
					/**
					 * Wrong format
					 */
					reportError( 'Response is not in expected format: ' + JSON.stringify( response ) );
				}
			})
			.fail ( function( xhr, ajaxOptions, thrownError )
			{
				/**
				 * Ajax error
				 */
				reportError( 'Ajax error: ' + xhr.statusText + ':' + thrownError );
			});
		});

		$("#btnShutdown").click(function(e)
		{
			if ( window.confirm("Are you sure?") )
			{
				$.ajax(
				{
					type: 'POST',
					url: '/admin/shutdownVM',
					data: {'action':'shutdown'},
					dataType: 'json',
				})
				.done ( function (response)
				{
					/**
					 * Test to see if our response is in the format we expect
					 */
					if (response.success)
					{
						if (response.success == "true")
						{
							/**
							 * Handle successful action
							 */
						}
						else
						{
							/**
							 * Handle Error or report
							 */
							reportError( JSON.stringify( response ) );
						}
					}
					else
					{
						/**
						 * Wrong format
						 */
						reportError( 'Response is not in expected format: ' + JSON.stringify( response ) );
					}
				})
				.fail ( function( xhr, ajaxOptions, thrownError )
				{
					/**
					 * Ajax error
					 */
					reportError( 'Ajax error: ' + xhr.statusText + ':' + thrownError );
				});
			}
			else
			{
				return false;
			}
			
		});
	});

	function reportError( strErrorMessage )
	{
		if (typeof intervalID != 'undefined') clearInterval(intervalID);
		console.log( strErrorMessage );
		$("#ajaxMessages").addClass( errorClass );
		$('#ajaxMessages').html( errorIcon + strErrorMessage );
		$("#ajaxMessages").show();
	}
</script>
<style></style>
<div id="AdminWrapper">
	<div id="AdminHeader"><h1>Shutdown VM</h1></div>

	<div id="AdminContent" class="admin">
		<p>Here you can shutdown or reboot the VM from the AMR dashboard.</p>
		<p><input type="button" id="btnShutdown" value="Shutdown" /><input type="button" id="btnRestart" value="Restart"/></p>

	</div>
</div>
<div id="ajaxMessages"></div>