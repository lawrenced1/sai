<?php
	$strTotalNumberOfMembers = '0';
	$strTotalNumberOfRecords = '0';
	$strTotalNumberOfRegistrations = '0';
	$strPercentRegistered = '0%';
	
?>
<script type="text/javascript">
	$(document).ready(function(e)
	{

	});
</script>
<style>
	#DashboardHeader
	{
		text-align: center;
		margin-left: auto;
		margin-right: auto;
	}

	.center
	{
		text-align: center;
	}
</style>
<div id="DashboardWrapper">
	<div id="DashboardHeader"><h1>Application Status</h1></div>

	<div id="DashboardContent" class="admin">
		<table>
			<tr>
				<td>Total Number of Records</td>
				<td class="rightJustify"><?php print (!empty($intRecordCount) ) ? $intRecordCount : $strTotalNumberOfRecords;?></td>
			</tr>
			<tr>
				<td>Total Number of Members</td>
				
				<td class="rightJustify"><?php print (!empty($intMemberCount) ) ? $intMemberCount : $strTotalNumberOfRecords;?></td>
			</tr>
			<tr>
				<td>Total Number of Registrations</td>
				<td class="rightJustify"><?php print (!empty($intRegisteredCount) ) ? $intRegisteredCount : $strTotalNumberOfRegistrations;?></td>
			</tr>
			<tr>
				<td>Percent Registered</td>
				<td class="rightJustify"><?php print $strPercentRegistered;?></td>
			</tr>
			<tr>
			</tr>
		</table>
	</div>
	<ul>
		<li>TODO:</li>
		<li>Make this page's numbers dynamic so it can be a true dashboard</li>
		<li>Start Training - sets the environment to developer</li>
		<li>Stop Training - set the environment to live</li>
		<li>Start Registration Button - unlock tables</li>
		<li>Stop Registration Button - lock tables in to view only.</li>
	</ul>
	<hr/>
	<ul>
		<li>Ideas:</li>
		<li>Networking Tab to handle static and DHCP and DNS</li>
		<li>Door prize accounting after registration closes?</li>
	</ul>
	<div class="center"><p><a href="/" target="_blank">Launch Application</a></p></div>
</div>