<script>

	$( document ).ready( function( e )
	{
		var serverIPAddress = '<?php print ( !empty( $strServerIPAddress ) ) ? $strServerIPAddress: 'Unknown'; ?>';

		$("#divServerIPAddress").html( 'Server IP Address:<b>' + serverIPAddress + '</b>');

		$( "ul.adminMenu li" ).on( "click", function (e)
		{
			if (typeof intervalID !== 'undefined') clearInterval( intervalID );

			menuBackgrounds( $(this).children().text() );

			$.ajax({
				url: $(this).find( "a" ).attr( "href" ),
				dataType : "text",
				beforeSend: function()
				{

				},
				success: function( response )
				{
					$(".adminContent").html(response);

				},
				error: function ( jqXHR, textStatus, errorThrown )
				{
					/**
					 * Error at the ajax level
					 */
					$(".adminContent").append( 'Server responded with: ', jqXHR.statusText + ': ' + errorThrown );
				}
			});

			e.preventDefault();
		});

		$('a[href="/admin/dashboard"]').trigger("click");

	});

	function menuBackgrounds( linkText )
	{
		$("ul.adminMenu li").each(function()
		{
			if ( linkText == $(this).children().text() )
			{
				$(this).find('a').css("background-color", "#555");
			}
			else
			{
				$(this).find('a').css("background-color", "#222222");
			}
		});
	}



</script>
<div class="container-fluid h-100">
	<div class="row adminHeader">
		<div class="col-sm-4"><img src="/sai/img/RegistrationIcon_24.gif" />Application Administration</div>
		<div id="divServerIPAddress" class="col-sm-8"></div>
	</div>
	<div class="row h-100">
		<div class="col-sm-2 adminMenu">
			<ul class="adminMenu">
				<li><a href="/admin/dashboard"><img src="/sai/img/icoDashboard.png" />Dashboard</a></li>
				<li><a href="/admin/settings"><img src="/sai/img/icoSettings.png" />Database Settings</a></li>
				<li><a href="/admin/importData"><img src="/sai/img/icoImport.png" />Import Data</a></li>
				<li><a href="/admin/criteria"><img src="/sai/img/icoCriteria.png" />Set Criteria</a></li>
				<li><a href="/replication/index"><img src="/sai/img/icoReplication.png" />Replication</a></li>
				<li><a href="/supervisors/index"><img src="/sai/img/icoSupervisors.png" />Set Supervisors</a></li>
				<li><a href="/admin/excludeAccounts"><img src="/sai/img/icoEmployees.png" />Set Exclusions</a></li>
				<li><a href="/printing"><img src="/sai/img/icoPrinting.png" />Printing</a></li>
				<li><a href="/reports/index"><img src="/sai/img/icoReports.png" />Reports</a></li>
				<li><a href="/export/index"><img src="/sai/img/icoExport.png" />Export Data</a></li>
				<li><a href="/tunnel"><img src="/sai/img/icoTunnel.png" />Support Tunnel</a></li>
				<li><a href="/admin/shutdownVM"><img src="/sai/img/icoShutdown.png" />Shutdown VM</a></li>
			</ul>
		</div>
		<div id="divAdminContent" class="col-sm-10 adminContent"></div>
	</div>
</div>