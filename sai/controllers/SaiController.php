<?php namespace sai\controllers; ?>
<?php if(stristr(__FILE__, $_SERVER['PHP_SELF'])) die(header('HTTP/1.0 404 Not Found'));?>
<?php

use nuCore\Controller;
use nuCore\Functions;
use nuCore\ErrorHandler;
use nuCore\JSON;
use nuCore\Janitor;

/**
 *
 */

class SaiController extends Controller
{
	/**
	 * Controller construction to allow overriding access types, variable setup, and views
	 * @param  {Registry}   \ Required registry object for handling dependency injection
	 * @param  {Model}      \ Model to be instantiated - usually the same name as the controller
	 * @param  {Controller} \ Controller name to be used
	 * @param  {strAction}  \ Method to be called when instantiating the object view
	 * @return {NULL}         Nothing is returned
	 */
	public function __construct( $objRegistry, $strModel=NULL, $strController=NULL, $strAction=NULL )
	{
		/**
		 * We need admin level access to modify tables and application settings.
		 */
		try
		{
			$aryDatabaseSettings = $objRegistry->get( 'database' );

			if( $aryDatabaseSettings === false ) throw new Exception( Debug::AddMessage( "No key found for database settings.", Debug::CONTROLLER ) );

			$aryDatabaseSettings['connectionName'] = 'admin';

			$objRegistry->set( 'database', $aryDatabaseSettings );

			/**
			 * Call parent construct with the new settings
			 */
			parent::__construct($objRegistry, $strModel, $strController, $strAction);
		}
		catch ( \Throwable $e )
		{
			Debug::AddMessage( ErrorHandler::PrintExceptionStack( $e ), Debug::CONTROLLER );
			Functions::Exit500( "Unable to access an admin registry setting." );
		}
	}


	public function index()
	{
		$this->_objView->set( 'strServerIPAddress', Functions::getServerAddress() );
	}

	public function dashboard()
	{
		Functions::prepareAjaxDisplay( $this->_objRegistry, $this->_objView );

		$objRegistration = new RegistrationsController( $this->_objRegistry, 'registration', 'Registrations', 'temp' );

		$objGetMemberCount = json_decode( $objRegistration->GetMemberCount('local') );

		$this->_objView->set('intMemberCount',
			( !empty( $objGetMemberCount->data[0]->memberCount ) )
			? $objGetMemberCount->data[0]->memberCount
			: 0);

		$objGetRegisteredCount = json_decode( $objRegistration->GetRegisteredCount('local') );

		$this->_objView->set('intRegisteredCount',
			( !empty( $objGetRegisteredCount->data[0]->registeredCount ) )
			? $objGetRegisteredCount->data[0]->registeredCount
			: 0);

		$objRecordCount = json_decode( $objRegistration->GetRecordCount('local') );

		$this->_objView->set('intRecordCount',
			( !empty( $objRecordCount->data[0]->recordCount ) )
			? $objRecordCount->data[0]->recordCount
			: 0);
	}

	public function settings()
	{
		Functions::prepareAjaxDisplay($this->_objRegistry, $this->_objView);
	}

	public function changeDataProvider($strProvider=NULL)
	{
		Functions::prepareAjaxDisplay($this->_objRegistry, $this->_objView);

		$objJSON = new JSON();

		if (empty($strProvider))
		{
			return print $objJSON
				->success('false')
				->errors('You must provide the data provider type in order to set the data provider value')
				->Response();
		}

		$this->_objRegistry->config['database']['import']['engine'] = $strProvider;

		try
		{
			$this->_objRegistry->config['database']['connectionName'] = 'application';
			$this->_objRegistry->writeConfiguration();
		}
		catch (Exception $e)
		{
			return print $objJSON
				->success('false')
				->errors('Unable to write the configuration changes to disk.')
				->Response();
		}

		return print $objJSON->success('true')
			->data('Successfully changed the data provider')
			->Response();
	}

	public function setupMemberServerAjax()
	{
		//Prepare Ajax response
		//Create JSON Object
		$objJSON = new JSON();

		//Call prepare
		Functions::prepareAjaxResponse($this->_objView);

		//Clean incomming variables
        $engine     = !empty($_POST['importEngine']) ? Janitor::sanitizeString($_POST['importEngine']) : '';
        $host       = !empty($_POST['importServerName']) ? Janitor::sanitizeString($_POST['importServerName']) : '';
        $database   = !empty($_POST['importDatabase']) ? Janitor::sanitizeString($_POST['importDatabase']) : '';
        $user       = !empty($_POST['importLogin']) ? Janitor::sanitizeString($_POST['importLogin']) : '';
        $password   = !empty($_POST['importPassword']) ? Janitor::sanitizeString($_POST['importPassword']) : '';


		if (empty($engine) || empty($host) || empty($database) || empty($user))
		{
			$objJSON->success('false')->data('Passed parameters cannot be NULL');
		}
		else
		{
			/**
			 * Confuguration placeholders created during setup routine
			 */
			$aryDatabaseSettings = $this->_objRegistry->get( 'database' );

			$aryDatabaseSettings['import']['engine'] = $engine;
			$aryDatabaseSettings['import']['servernameip'] = $host;
			$aryDatabaseSettings['import']['database'] = $database;
			$aryDatabaseSettings['import']['user'] = $user;
			$aryDatabaseSettings['import']['password'] = $password;

			try
			{
				$this->_objRegistry->set( 'database', $aryDatabaseSettings );
				//$this->_objRegistry->config['database']['connectionName'] = 'application';
				$this->_objRegistry->writeConfiguration();
				$objJSON->success('true')->data('Saved server configuration');
			}
			catch (Exception $e)
			{
				$objJSON
					->success('false')
					->errors($e->getMessage())
					->errors($e->getTraceAsString());
			}
		}
		return print $objJSON->Response();
	}//saveMemberServerAjax

	public function importData()
	{
		Functions::prepareAjaxDisplay($this->_objRegistry, $this->_objView);
	}

	public function importCSV($strHeaders = NULL)
	{
		$objJSON = new JSON();

		Functions::prepareAjaxResponse($this->_objView);

		$strInputFile = ROOT . DS . 'tmp' . DS . 'upload' . DS . 'import.csv';

		try
		{
			$this->_objModel->importCSV( $this->_objRegistry, $strInputFile );
		}
		catch (Exception $e)
		{
			return print $objJSON
				->success('false')
				->errors(__CLASS__.'->'.__FUNCTION__.' [' . __LINE__ . ']: Unable to import data from uploaded file.', 0, ErrorHandler::PrintExceptionStack($e))
				->Response();
		}

		return print $objJSON
			->success('true')
			->data('Successfully imported the uploaded file')
			->Response();


	}

	public function criteria()
	{
		Functions::prepareAjaxDisplay($this->_objRegistry, $this->_objView);

		$this->_objView->set( 'aryFields', $this->_objModel->getMemberColumnNames( $this->_objRegistry->config['database'][$this->_objRegistry->config['database']['connectionName']]['database'] ) );
	}

	public function getCriteria()
	{
		Functions::PrepareAjaxResponse( $this->_objView );

		$objJSON = new JSON();

		return print ( !empty( $this->_objRegistry->config['criteria'] ) )
			? $objJSON
				->success('true')
				->data( $this->_objRegistry->config['criteria'] )
				->Response()
			: $objJSON
				->success('true')
				->errors('')
				->Response();
	}

	public function saveCriteria()
	{
		Functions::prepareAjaxDisplay($this->_objRegistry, $this->_objView);

		$objJSON = new JSON();

		if ( $_SERVER['REQUEST_METHOD'] !== 'POST' ) return print $objJSON->success('false')->errors('Wrong method used')->Response();

		$this->_objRegistry->config['criteria'] = $_POST;

		try
		{
			$this->_objRegistry->config['database']['connectionName'] = 'application';
			$this->_objRegistry->writeConfiguration();
			return print $objJSON->success('true')->data()->Response();
		}
		catch (Exception $e)
		{
			Debug::AddMessage('Unable to write configuration', 3);
			return print $objJSON->success('false')->errors('Unable to write configuration')->Response();
		}

	}

	public function importLocalData()
	{
		Debug::AddMessage('Setting up response', 3);
		$objJSON = new JSON();

		/**
		 * Passing the registry and model name as expected by our framework for new controllers
		 */
		$objStatus = new StatusController($this->_objRegistry, 'status');

		Functions::PrepareAjaxResponse($this->_objView);

		try
		{
			$Result = $this->_objModel->importLocalData($this->_objRegistry, $objStatus);
			return $objJSON
				->success('true')
				->data($Result)
				->Response('print');
		}
		catch (Exception $e)
		{
			Debug::AddMessage(ErrorHandler::PrintExceptionStack($e), 1);
			return $objJSON
				->success('false')
				->errors('Error importing local data')
				->Response('print');
		}
	}

	public function shutdownVM()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'GET')
		{

		}
		elseif ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			Functions::PrepareAjaxResponse($this->_objView);

			$objJSON = new JSON();

			if ( !empty( $_POST['action'] ) )
			{
				if ( $_POST['action'] == 'restart')
				{
					$lastline = shell_exec('sudo /apps/bin/power-cycle 2>&1');
					Debug::AddMessage( Debug::VarDump( $lastline ) );
					if ($lastline)
					{
						return $objJSON->success('true')->data($lastline)->Response('print');
					}
					else
					{
						return $objJSON->success('false')->errors($lastline)->Response('print');
					}
				}

				if ($_POST['action'] == 'shutdown')
				{
					$lastline = shell_exec('sudo /apps/bin/power-down 2>&1');
					Debug::AddMessage( Debug::VarDump( $lastline ) );
					if ($lastline)
					{
						return $objJSON->success('true')->data($lastline)->Response('print');
					}
					else
					{
						return $objJSON->success('false')->errors($lastline)->Response('print');
					}
				}
			}
		}
	}//function

	public function processStatus()
	{
		Functions::PrepareAjaxResponse($this->_objView);

		$objJSON = new JSON;

		$objStatus = new StatusController($this->_objRegistry, 'status');

		try
		{
			return print $objJSON
				->success('true')
				->data
					(
						array
						(
							"max" => $objStatus->getMax(),
							"current" => $objStatus->getCurrent()
						)
					)
				->Response();
		}
		catch (Exception $e)
		{
			return print $objJSON->success('false')->errors('Unable to get current value')->Response();
		}
	}//function

	public function clearRegistrations()
	{
		if ( !DEVELOPMENT_ENVIRONMENT ) return Redirect::ToPage('/');

		try
		{
			if ( $this->_objModel->clearRegistrations() )
				Redirect::ToPage('/');

		}
		catch (Exception $e)
		{
			Debug::AddMessage( Debug::VarDump( $this->_objModel->getErrorInfo() ) );
			$this->_objView->set( 'strPhpMessages', 'Error clearing registrations ');
		}
	}

	public function replication()
	{

	}

	public function excludeAccounts()
	{

	}



	/*public function importRegistry( $strJSON )
	{
		$aryJSON = json_decode( $strJSON, true, 10 );

		if ( json_last_error() == JSON_ERROR_NONE )
		{
			$this->_objRegistry->config = $aryJSON;
			$this->_objRegistry->writeConfiguration();
		}
	}*/
}//class