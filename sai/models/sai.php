<?php namespace application\models; ?>
<?php if(stristr(__FILE__, $_SERVER['PHP_SELF'])) die(header('HTTP/1.0 404 Not Found'));?>
<?php

use nuCore\Model;
use nuCore\Debug;
use nuCore\ErrorHandler;

class Sai extends Model
{

	private $_outputFilePath;
	private $stmt;

	public function __construct($objDatabase = NULL)
	{
		//$this->_jsonFilePath = $root . $ds . 'public' . $ds . 'status' . $ds . 'status.json';
		$this->_outputFilePath = APP_UPLOADS . DS . 'dataExport.sql';
		parent::__construct($objDatabase);
	}

	public function importCSV( Registry $objRegistry, $strInputFile = NULL, $blnHeaders=FALSE )
	{
		if (empty($strInputFile)) throw new Exception(__CLASS__.'->'.__FUNCTION__.'['.__LINE__.']: Must pass a file to be imported.', 0);

		if (!is_readable($strInputFile)) throw new Exception(__CLASS__.'->'.__FUNCTION__.'['.__LINE__.']: The passed file cannot be read by AMR.', 0);

		//$strSQL = sprintf("LOAD DATA LOCAL INFILE '%s'", $strInputFile);
		//$strSQL .= " INTO TABLE registrations ";

		$strSQL = sprintf
		(
			'LOAD DATA INFILE \'%s\'
			INTO TABLE %s.members
			FIELDS TERMINATED BY \',\' OPTIONALLY ENCLOSED BY \'"\'
			LINES TERMINATED BY \'\\n\'
			IGNORE 1 LINES
			()'
			, $strInputFile
			, $objRegistry->config['database']['application']['database']
		);

		Debug::AddMessage( Debug::VarDump( $strSQL ) );

		try
		{
			if ( $this->_objDatabase->exec( $strSQL ) === false )
			{
				$strMessage = "Error importing Data via CSV.";
				Debug::AddMessage( $strMessage, 2 );
				throw new Exception( $strMessage, $e->getCode(), $e );
			}
		}
		catch (Exception $e)
		{
			$strMessage = "Error importing Data via CSV";
			Debug::AddMessage( $strMessage . PHP_EOL . ErrorHandler::PrintExceptionStack( $e ) );
			throw new Exception( $strMessage, $e->getCode(), $e);
		}
	}

	/**
	 * importLocalData
	 * @var objRegistry - Registry object for passing system configurations and unit testing
	 * @var objStatus - Status controller used for updating status table in the database to allow the UI to reflect status updates
	 *
	 * This function will import the downloaded local file into the current database. This will destory all data in the database!
	 */
	public function importLocalData(Registry $objRegistry, StatusController $objStatus)
	{

		Debug::AddMessage('Checking for object registry', 3);
		//Check if we have a registry object
		if (empty($objRegistry)) throw new Exception('Registry is NULL');

		try
		{
			$lineCount = Functions::FileLineCount($this->_outputFilePath);
		}
		catch (Exception $e)
		{
			throw new Exception('Unable to get line count', 0, $e);
		}

		Debug::addMessage('Line count = ' . $lineCount, 3);

		try
		{
			if ( $objStatus->SetMax( $lineCount ) === false )
			{
				$strMessage = "SetMax retuned false";
				Debug::AddMessage( $strMessage, 2 );
				throw new Exception( $strMessage );
			}
		}
		catch (Exception $e)
		{
			$strMessage = "Unable to set the max value on the status bar";
			Debug::AddMessage( $strMessage . PHP_EOL . ErrorHandler::PrintExceptionStack( $e ) );
			throw new Exception( $strMessage, $e->getCode(), $e);
		}

		try
		{
			/**
			 * We need to remove all print and registration data since the database has changed.
			 */

			if
			(
				$this->_objDatabase->exec
				(
					sprintf(
					'DELETE FROM %s.registrations'
					, Janitor::sanitizeString( $objRegistry->config['database'][ $objRegistry->config['database']['connectionName'] ]['database'] )
					)
				)
				=== FALSE
			)
			{
				throw new Exception( Debug::AddMessage( "Error deleting from registrations", 3 ) );
			}

			if
			(
				$this->_objDatabase->exec
				(
					sprintf(
					'DELETE FROM %s.printBatches'
					, Janitor::sanitizeString( $objRegistry->config['database'][ $objRegistry->config['database']['connectionName'] ]['database'] )
					)
				)
				=== FALSE
			)
			{
				throw new Exception( Debug::AddMessage( "Error deleting from registrations", 3 ) );
			}


			/**
			 * Important Note:
			 * Cannot use bind params with database names. Spent hours trying to figure out the problem.
			 * Since bindParams quotes the item, it causes a SQL syntax error.
			 */
			//Erase the existing data instead of dropping the table
			$strSQL = sprintf('DELETE FROM %s.members'
				, Janitor::sanitizeString( $objRegistry->config['database'][ $objRegistry->config['database']['connectionName'] ]['database'] )
			);

			if ( $this->_objDatabase->exec( $strSQL ) === FALSE )
			{
				Debug::AddMessage( 'Execute returned false : ' . $strSQL . json_encode( $this->_objDatabase->errorInfo() ), 1 );
				throw new Exception( 'Error executing delete from database. ' );
			}

			/**
			 * Need to capture SQL Errors too
			 * Testing if errorInfo() has any data
			 */
			$aryErrorInfo = $this->_objDatabase->errorInfo();
			if ( ! empty( $aryErrorInfo[1] ) )
			{
				Debug::AddMessage( 'Unable to delete the table. There was an error in the SQL itself. ' . json_encode( $aryErrorInfo ) );
				throw new Exception( 'Unable to delete the table.' );

			}
		}
		catch (Exception $e)
		{
			Debug::AddMessage( 'Error deleting old database ' . json_encode( $this->_objDatabase->errorInfo() ), 1 );
			throw new Exception('Unable to execute delete the old database.', 0, $e);
		}

		try
		{
			$objImportFile = new SplFileObject( $this->_outputFilePath, "r" );
		}
		catch (RuntimeException $e)
		{
			throw new Exception('Problem opening the import file', 0, $e);
		}

		$intRecordCount = 1;

		while (!$objImportFile->eof())
		{
			$strReadLine = $objImportFile->fgets();
			//debug::addMessage($strReadLine, 10, __FILE__, __CLASS__, __LINE__);

			if (!empty($strReadLine))
			{
				//We have a line, lets try and execute it
				try
				{
					if ( ! $this->_objDatabase->exec( $strReadLine ) )
					{
						Debug::AddMessage( 'Error inserting line ' . $strReadLine . json_encode( $this->_objDatabase->errorInfo() ), 1 );
						throw new Exception('Error inserting record into database.' );
					}
				}
				catch (Exception $e)
				{
					throw new Exception('Unable to execute SQL ', 0, $e);
				}

				//Increment our counter
				++$intRecordCount;
				if ( $intRecordCount % 1000 == 0 ) $objStatus->SetCurrent( $intRecordCount );
			}
		}

		return $intRecordCount;
	}//function

	public function getMemberColumnNames( $strDatabaseName = 'amr' )
	{
		/**
		 * Validate passed Variables
		 */
		if ( empty( $strDatabaseName ) )
		{
			$strMessage = "DatabaseName cannot be NULL";
			Debug::AddMessage( $strMessage, 2 );
			throw new Exception( $strMessage );
		}

		/**
		 * Sanitize inputs used here
		 */
		if ( ! $strDatabaseName = Janitor::sanitizeString( $strDatabaseName ) )
		{
			$strMessage = "DatabaseName failed sanitization";
			Debug::AddMessage( $strMessage . "[" . $strDatabaseName . ']', 2 );
			throw new Exception( $strMessage );
		}

		try
		{
			$this->stmt = $this->_objDatabase->query( 'SHOW FIELDS FROM `'. $strDatabaseName . '`.members');
			return $this->stmt->fetchAll();
		}
		catch (Exception $e)
		{
			Debug::AddMessage( "Unable to get the member column names" . ErrorHandler::PrintExceptionStack( $e ) , 3 );
			throw new Exception('Unable to get the member column names', $e->getCode(), $e);
		}
	}

	public function clearRegistrations( $strDatabaseName = 'amr' )
	{
		$strSQL = "DELETE FROM `$strDatabaseName`.registrations";

		try
		{
			$this->_objDatabase->exec( $strSQL );
			return TRUE;
		}
		catch (Exception $e)
		{
			Debug::AddMessage( 'Unable to delete from registrations' . ErrorHandler::PrintExceptionStack( $e ), 3 );
			throw new Exception('Unable to delete from registrations', $e->getCode(), $e);
		}
	}

	public function getErrorInfo()
	{
		if ( isset( $this->stmt ) )
		{
			return $this->stmt->errorInfo();
		}
		else
		{
			return $this->_objDatabase->errorInfo();
		}
	}

}//class

/*For Importing an uploaded CSV into mysql
$query = <<<eof
    LOAD DATA INFILE '$fileName'
     INTO TABLE tableName
     FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
     LINES TERMINATED BY '\n'
    (field1,field2,field3,etc)
eof;

$db->query($query);
*/